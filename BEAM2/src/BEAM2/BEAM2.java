package BEAM2;

//MULTI RUN BRANCH
//___
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
//import java.io.Console;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class BEAM2 {
	private static boolean acceptScore(double scoreVecchio, double scoreNuovo, double temperature) {
		if (scoreNuovo-scoreVecchio >0) System.err.println("SPOTTED SOME DISCREPANCIES!! Score mandato in"
				+ "accettazione quando non doveva");
		double prob = Math.exp(1*(scoreNuovo - scoreVecchio)/temperature);
		if (temperature == 0){
			prob = 0;
		}
		if(Math.random() < prob ){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * @param args
	 */
	@SuppressWarnings("unused")
	public static MotifMemory run(String[] args, int idx, int mask, String output, ArrayList<Motif> inputSequences) {
		long startTime = System.currentTimeMillis();

		//ArrayList<Motif> inputSequences=new ArrayList<Motif>();

		MotifUtilities.reshapeInput(inputSequences);
		
		int check = 0;
		int escape = 0;

		//IO.readInput(args[0],inputSequences); //riempie inputSequences
		//creazione branch develop
		double[][]mbr=new double[83][83];
		double[][]condProb = new double[83][83];
		double[]dataPriors = new double[83];
		double[]weights = new double[83];

		BEARManager.readMatrix("./src/matrixPriori", condProb);
		BEARManager.readMatrix("./src/matriceRidondanza50",mbr); //riempie mbr
		BEARManager.readVector("./src/listaPriorPesati_uniform.txt", dataPriors);
		BEARManager.readVector("./src/weights.txt", weights);


		MotifManager manager=new MotifManager();

		//default
		int motifWidth=10;
		int startingNoOfSequences = 2;
		//		double percentage =0.01;

		double temperatureStart=100;
		double coolingRate=0.001;//These two variables can be fixed values or can be taken as input
		double minTemp = 0.001;


		int widthUpperBound = 100;

		int minSteps = 10000;

		double passiTot=1; //poi cambia

		int escapeCondition = 1;



		final int N = 5; //numero medio di operazioni per sequenza:controlla il numero minimo di passi
		//CommandLine Parser 
		CommandLineParser clp = new CommandLineParser(args);
		//initialized CLP, the hash is created, check with - GetValue(key), ContainsKey(key)

		//do not touch
		double maximumScoreEver=-9999;
		boolean escaped = false;
		double temperature = temperatureStart;
		boolean clean = false; //parziali > 50% media
		boolean superClean = false; //parziali >0
		double bonusSeq = 0.0;
		double nThr = .3;
		boolean branching = true;
		boolean weHaveSubopt = false;

		//-----LETTURA COMMAND LINE ARGUMENTS (CLA)
		if(clp.containsKey("s")) minSteps=Integer.parseInt(clp.getValue("s")); 			//numero min di passi prima di fermarsi
		if(clp.containsKey("W")) widthUpperBound=Integer.parseInt(clp.getValue("W")); 	//limite superiore larghezza motif
		if(clp.containsKey("w")) motifWidth=Integer.parseInt(clp.getValue("w")); 		//starting motif width
		if(clp.containsKey("T")) temperature=Double.parseDouble(clp.getValue("T")); 	//starting T
		if(clp.containsKey("r")) coolingRate=Double.parseDouble(clp.getValue("r")); 	//cooling rate, precision
		/*if(clp.containsKey("f")) {
			IO.readInput(clp.getValue("f"), inputSequences);
		}else{
			IO.readInput(args[0],inputSequences);
		} //riempie inputSequences
		 */
		if(clp.containsKey("n")) startingNoOfSequences=Integer.parseInt(clp.getValue("n")); //sampling iniziale
		if(clp.containsKey("o")) output=clp.getValue("T"); 								//output folder
		if(clp.containsKey("C") && clp.getValue("C").equals("1")) {clean=true;}			
		else if(clp.containsKey("C") && clp.getValue("C").equals("2")){clean=true;superClean=true;}
		if(clp.containsKey("N")) bonusSeq=Double.parseDouble(clp.getValue("N"));		//BONUS
		//if(clp.containsKey("N")) bonusSeq=Integer.parseInt(clp.getValue("N"));
		if(clp.containsKey("t")) nThr=Integer.parseInt(clp.getValue("t"));				//Threshold identità
		if(clp.containsKey("b")) {														
			if (clp.getValue("b").equals("F")) branching=false;
		}
		if(clp.containsKey("m") && clp.getValue("m").equals("T")) weHaveSubopt = true;
		//----------
		//		percentage =startingNoOfSequences/(double)inputSequences.size();

		if (!branching){
			MotifUtilities.setBranchingZero(mbr);
			if (Debug.VERBOSE){
				System.out.println("Setting :-: equal to 0...mbr[:][:]: " + BEARManager.substitutionScore(':', ':', mbr));
			}
		} else {
			if (Debug.VERBOSE){
				System.out.println("branch to branch substitution considered: " + BEARManager.substitutionScore(':', ':', mbr));
			}
		}


		coolingRate = (temperatureStart - minTemp)/minSteps; //in pratica quando la T va a 0.0


		//-----QUI FINISCONO I PARAMETRI DI INPUT-----

		//		System.out.println(motifWidth + " " + inputSequences.size() + " " + percentage);

		MotifHandler mh = null;
		MotifHandler bestMh = new MotifHandler();

		//TODO gestire run multiple
		if ( mask == 1 && idx == 1){
			//System.out.println(inputSequences.size());
			mh=manager.initialise(motifWidth,inputSequences,mbr, startingNoOfSequences);//genera la prima soluzione casuale		
		}else{
			mh=manager.initialise2(motifWidth,inputSequences,mbr,startingNoOfSequences);
		}
		if (Debug.VERBOSE) {System.out.println("---GENERATED STARTING MH---\n"+mh.toString());}
		//---------------------
		PSSM.computePSSM(mh, inputSequences);
		mh.setMotifWidth();		

		if (Debug.VERBOSE){
			if (mh.getObjectMotif(0).getNucleotides().equals("")){
				System.err.println("Nucleotide sequences not found");
				bonusSeq = 0.0;
			}
		}
		
		if (Debug.ON) Debug.checkRNA(mh);
		MotifUtilities.computePartials(mh.getPSSM(), mh, mbr, mh.getMotifWidth(), bonusSeq);
		MotifHandler.computeScore(mh, weights, dataPriors, bonusSeq, nThr);//genera primo punteggio
		double prevScore = mh.getScore();
		int contatore=0;

		//per avere almeno N operazioni su ogni sequenza in media (7 operazioni, X subopt)
		passiTot = inputSequences.size()*7*mh.getObjectMotif(0).getSequenceList().size()*N; 		

		if (passiTot < minSteps){
			passiTot = minSteps + inputSequences.size()*7*mh.getObjectMotif(0).getSequenceList().size();
		}

		//CLA
		if(clp.containsKey("S")) passiTot=Double.parseDouble(clp.getValue("S")); //force passiTot
		//-----------

		escapeCondition = Math.max((int)(passiTot*.005),1000); //anche in un dataset piccolo richiediamo almeno 1000 steps per scappare



		//se dopo aver in media agito con ogni perturbazione su tutte le sequenze una volta non cambia nulla, allora escape, ma solo se sono stati fatti almeno minSteps

		if (Debug.VERBOSE) {
			System.out.println("..............STARTING MOTIF..............\n");
			System.out.println(mh.toString());
			System.out.println("Score : " + mh.getScore());
			System.out.println("Length : " + mh.getMotifWidth());
			System.out.println("#Sequences : " + mh.cardinality() +"\n");
		}

		/*---------*/
		double[][] media;
		media = new double[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
		int[][] count;
		count = new int[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
		int[][] acceptSecondCount;
		int[][] rejectCount;
		acceptSecondCount = new int[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
		rejectCount = new int[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};


		double currentScore=mh.getScore();
		//-----INIZIO CICLO -----

		while((escape < escapeCondition || contatore < minSteps) && contatore < passiTot ){
			//se anche e' stata raggiunta la condizione di escape non fermarti prima di minSteps, in ogni caso esci dopo passiTot
			if (Debug.VERBOSE) {System.out.println("\n>>>>>>>>>>>>>>>ITERATION : "+contatore+ " di " + passiTot + "<<<<<<<<<<<<<<<");}
			currentScore=mh.getScore();
			//System.out.println(perturbatedMotif.motifLength + "\t" + m.motifLength);

			if(mh.cardinality() >= 1){
				mh.setMotifWidth();
			}else{
				//				System.err.println(mh.cardinality());
			}

			//-----PERTURBO IL MOTIVO, TENGO LA SOLUZIONE PRECEDENTE-----
			int operation = manager.perturbateMotif(mh,inputSequences,mbr, widthUpperBound, weHaveSubopt);
			//			if (operation == -1){
			//				IO.scream(-1);
			//			}
			//-----NEL CASO NORMALE CALCOLA PSSM E SCORE-----
			if(mh.cardinality()<1){
				//-----SE IL MH CONTIENE 0 SEQUENZE INVECE FAI UN ADD-----		
				currentScore=0;
				mh.setMotifWidth(motifWidth);
				operation = manager.perturbateMotif(mh,inputSequences,mbr, widthUpperBound, weHaveSubopt);
			}
			if (operation == -1){
				break;
			}
			//SE CI SONO PROBLEMI DECOMMENTARE QUESTO SETMOTIFWIDTH ALERT ALERT ALERT!
			//mh.setMotifWidth();
			
			//DEBUGGONE per errori di coding - se trova sequenze sballate le ri-sposta tra le input
			ArrayList<Motif> outOfPlaceMotifs = Debug.checkStartEnd(mh, inputSequences);
			if (Debug.ON && outOfPlaceMotifs.size() != 0){
				for(Motif nome: outOfPlaceMotifs) System.err.println(nome.getName() + "\twrong width");
			}
			ArrayList<Motif> maskedMotifs=Debug.checkMasks(mh, inputSequences);
			if (Debug.ON && maskedMotifs.size() != 0){
				for(Motif nome: maskedMotifs) System.err.println(nome.getName() + "\tmotif in mask");
			}

			if (Debug.VERBOSE){
				System.out.println(mh.toString());
//				if (mh.cardinality() > 4){
//					System.out.println(inputSequences.size());
//					System.out.println();
//				}
			}
			PSSM.computePSSM(mh, inputSequences);
			MotifUtilities.computePartials(mh.getPSSM(), mh, mbr, mh.getMotifWidth(), bonusSeq);
			MotifHandler.computeScore(mh, weights, dataPriors, bonusSeq, nThr);

			/*Test per peso singole operazioni*/
			double delta = mh.getScore() - currentScore;

			if (contatore < minSteps){
				media[(int)(contatore/(minSteps/3.0))][operation] += delta;
				count[(int)(contatore/(minSteps/3.0))][operation] += 1;
			}
			/*-------------------------------*/
			double newScore = mh.getScore();

			//			//correzione per accettazione nuova colonna lo fa solo per expand e shrink
			//			if(operation == 4){
			//				newScore = mh.getScore()*((double)mh.getMotifWidthPrev()/(mh.getMotifWidthPrev()+1.0));
			////				newScore -= correction;
			//				System.out.println("new:" + newScore + " score: " + mh.getScore());
			//				System.out.println("current:" + currentScore);
			//			}else if(operation == 5){
			//				newScore = mh.getScore()*(((double)mh.getMotifWidthPrev()+1.0)/mh.getMotifWidthPrev());
			////				newScore += 2*correction;
			//
			//				System.out.println("new:" + newScore + " score: " + mh.getScore());
			//				System.out.println("current:" + currentScore);
			//			}



			if(newScore>=currentScore){
				if (Debug.VERBOSE) {System.out.println("---ACCEPTED---\n");}
				currentScore=mh.getScore();

			}else if(acceptScore(currentScore,newScore, temperature)){
				if (Debug.VERBOSE) {System.out.println("---ACCEPTED SECOND CHANCE---\n");}				
				currentScore=mh.getScore();

				acceptSecondCount[(int)(contatore/(passiTot/3))][operation] += 1;
			}else{
				if (Debug.VERBOSE) {System.out.println("\n---REJECTED---\n");}
				MotifManager.ctrlZ(operation, mh, inputSequences);//il ctrlZ non riprende il vecchio score!!! ora si
				mh.getPSSM().setScore(currentScore); //aggiunto : maxScore e' semplicemente lo score del giro precedente

				if (contatore<minSteps-1){
					rejectCount[(int)(contatore/minSteps*3)][operation] += 1;
				}

			}


			if (Debug.VERBOSE) {	System.out.println("---cooling down---\n");
			}
			if(temperature > minTemp){
				temperature-=coolingRate;
			}else{
				temperature = 0;
			}


			//----------------------------TEST MAX SCORE----------------------------


			if(mh.getScore() >= prevScore){
				//se migliora lo score, non aumentare l'escape, anzi azzeralo. A meno che non sia aumentato di troppo poco.
				maximumScoreEver = mh.getScore();

				bestMh.MotifHandlerClone(mh);
				bestMh.getPSSM().setScore(maximumScoreEver); 

				check = contatore;
				//System.err.println(Math.abs(mh.getScore() - maximumScoreEver));

				int threshold = 1;
				if(Math.abs(mh.getScore() - prevScore) < threshold){ //TODO da stimare meglio
					escape++;
					//System.err.println("escape in: " + (escapeCondition - escape) + " passi");
				}else{
					escape = 0;
				}
			}
			prevScore = mh.getScore();
			//----------------------------TEST MAX SCORE END----------------------------


			if(mh.cardinality() >= 1){
				mh.setMotifWidth();
			}
			//			System.out.println(mh.toString());
			if (Debug.VERBOSE) {
				System.out.println("Temperature : " + temperature);
				System.out.println("Score : " + mh.getScore());
				System.out.println("Length : " + mh.getMotifWidth());
				System.out.println("#Sequences : " + mh.cardinality() +"\n");
			}
			contatore++;
			//			if(contatore%100 == 0){
			//				//				System.out.println(Math.round(contatore/passiTot*100) + "%");
			//			}

		}
		System.out.println("\n-------------END OF COMPUTATION-------------\n");


		if(escape >= escapeCondition) escaped = true;

		PSSM.computePSSM(mh,inputSequences);
		
		if (bestMh.cardinality() == 0){
			bestMh.MotifHandlerClone(mh);
			PSSM.computePSSM(bestMh, inputSequences);
			bestMh.getPSSM().setScore(mh.getScore());
		}else{
			PSSM.computePSSM(bestMh, inputSequences);
		}
		
		if (Debug.VERBOSE){
			System.out.println("Mh PSSM --------------------");
			System.out.println(mh.printPSSM());
			System.out.println("BestMh PSSM --------------------");
			System.out.println(bestMh.printPSSM());

		}


		MotifUtilities.computePartials(mh.getPSSM(), mh, mbr, mh.getMotifWidth(), bonusSeq);
		MotifUtilities.computePartials(bestMh.getPSSM(), bestMh, mbr, bestMh.getMotifWidth(), bonusSeq);

		//Pulizia --------- (--clean <- non funge? -C? non funge?)
		System.out.println("clean: " + clean);

		if (clean){
			System.out.println("\n[-------------CLEANING INITIATED-------------]\n");

			double thr = 0.0;
			if(superClean){
				thr = 0.5;
			}
			MotifUtilities.clean(mh, thr);
			MotifUtilities.clean(bestMh, thr);

			PSSM.computePSSM(mh, inputSequences);
			PSSM.computePSSM(bestMh, inputSequences); //rifaccio dopo il clean


			System.out.println("Motif Score : "+mh.getScore());
			System.out.println("BMotif Score : "+bestMh.getScore());

			MotifHandler.computeScore(mh, weights, dataPriors, bonusSeq, nThr);
			MotifHandler.computeScore(bestMh, weights, dataPriors, bonusSeq, nThr);

			System.out.println("Motif Score after Cleaning : "+mh.getScore());
			System.out.println("BMotif Score after Cleaning : "+bestMh.getScore());

			System.out.println("\n[-------------CLEANING COMPLETED-------------]\n");
		}
		//-----------------------
		//		System.out.println("Motif------");
		//		System.out.println(mh.toString());
		//		System.out.println("------");
		//		System.out.println("Motif Score : "+mh.getScore());
		//		System.out.println("------");
		//		if (mh.getObjectMotif(0).getNucleotides().equals("") == false){
		//			System.out.println(mh.toStringSeq());
		//		}

		if(mh.cardinality() != 0){
			mh.setMotifWidth();}

		//		System.out.println(mh.printPSSM());

		//Media Delta Score
		if (Debug.ON){ 
			System.out.println("\n-------------Medie Delta Score-------------\n");
			for(int parte = 0; parte < 3; parte++){
				for(int k = 0; k < media[parte].length; k++){
					media[parte][k] /= count[parte][k];
				}
			}	

			for (int phase = 0; phase<=2; phase++){
				if(phase == 0){
					System.out.println("\nfase high T-----");
				}else if(phase == 1){
					System.out.println("\nfase mid T-----");
				}else{
					System.out.println("\nfase low T-----");
				}
				for (int op = 0; op<=5; op++){
					if (op == 0){
						System.out.print("media add: ");
					}else if(op == 1){
						System.out.print("media remove: ");
					}else if(op == 2){
						System.out.print("media shift: ");
					}else if(op == 3){
						System.out.print("media recalculate: ");
					}else if(op == 4){
						System.out.print("media expand: ");
					}else if(op == 5){
						System.out.print("media shrink: ");
					}
					System.out.println(media[phase][op] + "\t#operazioni: " + count[phase][op] +
							"\taccept: " + (count[phase][op]-acceptSecondCount[phase][op]-rejectCount[phase][op]) + "\taccept2nd: " + acceptSecondCount[phase][op] + 
							"\treject: " + rejectCount[phase][op]);
				}
			}
		}
		
		System.out.println("\n----- final local alignment -----\n");
		//riportare comunque il maximum score ever
		if(maximumScoreEver > mh.getScore()){
			
				System.out.println(bestMh.toString());
			if (mh.getObjectMotif(0).getNucleotides().equals("") == false){
				System.out.println(bestMh.toStringSeq());
			}
			System.out.println("\nSequences: " + bestMh.cardinality());
			System.out.println("\nbestMh score: " + maximumScoreEver);

		}else{
			bestMh.getPSSM().setScore(mh.getScore());
			System.out.println(bestMh.toString());
			if (mh.getObjectMotif(0).getNucleotides().equals("") == false){
				System.out.println(bestMh.toStringSeq());
			}
			System.out.println("\n\nn.Sequenze: " + bestMh.cardinality());
			System.out.println("\n\nscore bestMh: " + bestMh.getScore());			
		}

//		if (mh.getObjectMotif(0).getNucleotides().equals("") == false){
//			System.out.println("\n#nucleotide conservation (given as the fraction of conserved columns (>70%)\t" + PSSM.checkBonusSeq(bestMh));
//		}

		String fileBench="lastrun_m" + mask + "_run"  + idx ;
		if (clp.containsKey("f")){
			String[] tmp =  clp.getValue("f").split("/");
			fileBench = tmp[tmp.length-1].split("\\.")[0]+ "_m" + mask + "_run"  + idx; //
		}

		//WebLogo

		String webLogoOut = output + "/webLogoOut/mask"+ mask +"/" + fileBench + "_wl.fa"; //output da dare a weblogo
		boolean webLogo=false;
		if(webLogo){
			System.out.println(bestMh.toString(webLogo,true));
		}	

		Writer writer = null;

		try {
			writer = new BufferedWriter(new OutputStreamWriter(

					new FileOutputStream(webLogoOut), "utf-8"));

			
			writer.write(bestMh.toString(webLogo, true));

		} catch (IOException ex) {
		} finally {
			try{writer.close();} catch (Exception ex) {}
		}



		String pathBench = output + "/benchmark/mask" + mask + "/" + fileBench + ".txt";
		try {

			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(pathBench), "utf-8"));


			writer.write(bestMh.toString());
			writer.write("\n#otherMatches\n");
			writer.write(bestMh.otherMatches());

			if (mh.getObjectMotif(0).getNucleotides().equals("") == false){
				writer.write("\n#Seq PSSM\n");
				writer.write(bestMh.toStringSeq());			
			}


			writer.write("\n#PSSM\n");
			writer.write(bestMh.printPSSM());
			writer.write("\n#score\t" + bestMh.getScore());
			writer.write("\n#seq\t" + bestMh.cardinality());
			writer.write("\n#width\t" + bestMh.getMotifWidth());
//			if (mh.getObjectMotif(0).getNucleotides().equals("") == false){
//				writer.write("\n#nucCons\t" + PSSM.checkBonusSeq(bestMh));
//			}else{
//				writer.write("\n#nucCons\tNULL");
//			}
			writer.write("\n#escape\t" + escaped);
			writer.write("#onStep\t" + contatore);
			writer.write("#minSteps\t" + minSteps);
			writer.write("#maxSteps\t" + passiTot);


		} catch (IOException ex) {
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}



		//file da passare al jar che calcola lo score, per i test

		String pathScoreTest = output + "/scoreTest/mask" + mask + "/" + fileBench + ".txt";

		try {

			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(pathScoreTest), "utf-8"));


			writer.write(bestMh.toString2());


		} catch (IOException ex) {
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}
		String pathScoreExcel= output +"/excel/mask"+ mask + "/excel_"+idx+".txt";
		try {

			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(pathScoreExcel), "utf-8"));


			writer.write(bestMh.toStringExcel());


		} catch (IOException ex) {
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}
		
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("run time: " + ((int)elapsedTime/10)/100.0 + " s");

		return new MotifMemory(bestMh.getScore(), bestMh);

	}

	public static void applyMask(MotifHandler mh){
		for (Motif m: mh.getListMotif()){
			m.addMask(m.getMotifStart(), m.getMotifEnd());
			//inputSequences.add(m); //no perch� alla fine deve mascherare con la migliore ma riepire dell'ultima
		}
	}

	public static void refillInput(MotifHandler mh, ArrayList<Motif> inputSequences){
		if (Debug.ON){
			System.out.println("input size: "+ inputSequences.size());
			System.out.println("mh size: " + mh.cardinality());
			System.out.println();
		}
		for (Motif m: mh.getListMotif()){
			inputSequences.add(m);
		}
	}

	public static void main(String[] args) throws IOException {

		//inputSequences
		ArrayList<Motif> inputSequences=new ArrayList<Motif>();


		int numberOfRuns = 1;
		int numberOfMasks=1;
		String baseName = "lastrun";
		//CommandLine Parser 
		CommandLineParser clp = new CommandLineParser(args);
		//initialized CLP, the hash is created, check with - GetValue(key), ContainsKey(key)
		if(clp.containsKey("R")){
			numberOfRuns = Integer.parseInt(clp.getValue("R"));
		}
		if(clp.containsKey("f")){
			String[] tmp =  clp.getValue("f").split("/");
			baseName = tmp[tmp.length-1].split("\\.")[0]; //
			IO.readInput(clp.getValue("f"), inputSequences);
		}


		String outFolder="risultati/" + baseName;
		final File dir = new File(outFolder);
		if(dir.exists()){

			System.out.println("My scans tell me I already own an output with this name -"+baseName+ "-, want me to overwrite it?");

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
			String response = bufferedReader.readLine();
			//String response = console.readLine("y/n");
			if(response.toLowerCase().equals("y")){
				System.out.println("OVERWRITING...");
			}else{
				System.out.println("CLOSING...");
				System.exit(0);
			}
		}
		String outBench=outFolder+"/benchmark";
		String outScore=outFolder+"/scoreTest";
		String outLogo=outFolder+"/webLogoOut";
		dir.mkdirs();
		new File(outBench).mkdirs();
		new File(outScore).mkdirs();
		new File(outLogo).mkdirs();


		for(int mask=1; mask<=numberOfMasks; mask++){
			for(int i=1; i<=numberOfRuns; i++){
				run(args, i,mask, outFolder, inputSequences);
				System.out.println("run " + i + " completed");
			}
		}
	}


}

