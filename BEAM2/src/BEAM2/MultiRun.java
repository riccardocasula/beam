package BEAM2;

//
//Dear maintainer:
//	
//When I wrote this code, only I and Eugenio and God 
//knew what it was. 
//
//Now, only God knows!
//
//So if you are done trying to 'optimize' 
//this routine (and failed),
//please increment the following counter
//as a warning
//to the next guy:
//	
//total_hours_wasted_here = 0
//
//Sincerely, 
//Marco "Noise" Pietrosanto
//

//import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
//import java.io.InputStreamReader;
import java.util.ArrayList;

public class MultiRun extends BEAM2 {

	public static void main(String[] args) throws IOException {
		MotifMemory mm = new MotifMemory();
		MotifMemory tmpMm = new MotifMemory();
		//genero inputSequences
		ArrayList<Motif> inputSequences=new ArrayList<Motif>();

		int numberOfRuns = 10;
		int numberOfMasks = 1;
		String baseName = "lastrun";
		//CommandLine Parser 
		CommandLineParser clp = new CommandLineParser(args);
		//initialized CLP, the hash is created, check with - GetValue(key), ContainsKey(key)
		if(clp.containsKey("R")){								//Runs
			numberOfRuns = Integer.parseInt(clp.getValue("R"));
		}
		if(clp.containsKey("M")){								//# di mask
			numberOfMasks = Integer.parseInt(clp.getValue("M"));
		}
		if(clp.containsKey("f")){								//File input
			String[] tmp =  clp.getValue("f").split("/");
			baseName = tmp[tmp.length-1].split("\\.")[0]; //
			IO.readInput(clp.getValue("f"), inputSequences);
		}
		if(clp.containsKey("v")){
			Debug.setVERBOSE(true);
		}


		String outFolder="risultati/" + baseName;
		final File dir = new File(outFolder);
/*		if(dir.exists()){

			System.out.println("My scans tell me I already own an output with this name -"+baseName+ "-, want me to overwrite it?");

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
			String response = bufferedReader.readLine();
			//String response = console.readLine("y/n");
			if(response.toLowerCase().equals("y")){
				System.out.println("OVERWRITING...");
				IO.deletePrevious(outFolder);
			}else{
				System.out.println("CLOSING...");
				System.exit(0);
			}
		} 
		*/
		String outBench=outFolder+"/benchmark";
		String outBest = outBench + "/motifs";
		//String outScore=outFolder+"/scoreTest";
		String outLogo=outFolder+"/webLogoOut";
		String outBestLogo= outLogo + "/motifs";
		String outExcel= outFolder + "/excel";
		String outBestExcel= outExcel + "/motifs";
		dir.mkdirs();

		new File(outBench).mkdirs();
		new File(outBest).mkdirs();
		//new File(outScore).mkdirs();
		new File(outLogo).mkdirs();
		new File(outBestLogo).mkdirs();
		new File(outExcel).mkdirs();
		new File(outBestExcel).mkdirs();

		int bestRunIdx=0;

		long startTime = System.currentTimeMillis();

		
		for(int mask=1; mask<=numberOfMasks; mask++){

			ArrayList<Motif> clonedInput = new ArrayList<Motif>();
			mm=null;
			mm=new MotifMemory();

			tmpMm = null;
			tmpMm =new MotifMemory();

			String outMask = outBench + "/mask" + mask;
			String outMaskLogo = outLogo + "/mask" + mask;
			String outMaskExcel = outExcel + "/mask" + mask;
			new File(outMask).mkdirs();
			new File(outMaskLogo).mkdirs();
			new File(outMaskExcel).mkdirs();
			
			for(int i=1; i<=numberOfRuns; i++){
				//DEBUG
				//				System.out.println(inputSequences.size());
				//				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
				//				String response = bufferedReader.readLine();
				//

				//				System.out.println("size input: " + inputSequences.size());
				tmpMm=run(args, i, mask, outFolder, inputSequences);

				if(mm.tryMask(tmpMm.getScore(), tmpMm.getHandlerMemory())){
					bestRunIdx = i;
					clonedInput = new ArrayList<Motif>(inputSequences.size()); //Se la maschera e' l'attuale migliore, segnati anche lo stato di input sequences
					for(Motif motif: inputSequences){
						clonedInput.add(new Motif(motif));
					}
				}

				if(i != numberOfRuns){ //altrimenti all'ultima run della maschera refilla due volte -- all'ultimo va refillato col migliore
					refillInput(tmpMm.getHandlerMemory(), inputSequences);
				}
				//outFolder � basename/
				System.out.println("mask: " + mask +"\trun: " + i + "\tcompleted");
			}
			//in bestRunIdx e' contenuto l'indice della run da copiare, in mask c'e' invece il numero di maschera

			if(mask != numberOfMasks){
				System.out.println("applying mask");
			}

			//System.out.println(inputSequences.get(0).getMaskList().size());

			applyMask(mm.getHandlerMemory());
			refillInput(mm.getHandlerMemory(), clonedInput); //Applica la maschera ai Motif del migliore mh e riempi l'inputSequences associato ad esso con i motif 
			//aggiornati

			inputSequences = null; //svuota inputSequences e riempila copiando gli elementi di ClonedInput
			inputSequences = new ArrayList<Motif>(clonedInput.size());
			for(Motif motif: clonedInput){
				inputSequences.add(new Motif(motif));
			}
			/*
			for(Motif m: inputSequences){
				System.out.println(m.printMask());
			}
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
			String response = bufferedReader.readLine();
			//*/
			
			//COPIA cartelle
			IO.moveBestRuns(baseName, mask, bestRunIdx, outBench);
			IO.moveBestRunsLogo(baseName, mask, bestRunIdx, outLogo);
			IO.moveBestExcel(baseName, mask, bestRunIdx, outExcel);
		}

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("full run time: " + ((int)elapsedTime/10)/100.0 + " s");
		System.out.println("risultati contenuti in " + outFolder);

	}//fine main

}
