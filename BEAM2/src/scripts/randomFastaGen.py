import random
loop = "lmnopqrstuvwxyz^"
stem = "abcdefghi=ABCDEFGHIJ"
i_loop = "!\"#$%&'()+KLMNOPQRSTUVW234567890YZ~?_|/\\@"
bulge = "[]{}"

bearStr=loop+stem+i_loop+bulge

for i in range(1,1001):
	print ">" + str(i)
	seq = ""
	for letter in range(0,50):
		idx = random.randint(0,len(bearStr)-1)
		seq += bearStr[idx]
	print seq
