package BEAM2;

import java.io.BufferedReader;
import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
//import java.nio.channels.FileChannel;
//import javax.swing.plaf.basic.BasicBorders.SplitPaneBorder;



import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;

public class IO {

	static void readInput(String path, ArrayList<Motif> inputSequences){
		FileReader f = null;
		try{
			//apro file
			f = new FileReader(path);
		}catch (IOException ioException){
			ioException.printStackTrace();
		}

		try{
			BufferedReader reader = new BufferedReader(f);
			String s,name="",seq="", firstName="", nuc="", dotB="";
			boolean start=true;
			boolean subopt = false;

			while ((s=reader.readLine())!=null){

				//System.out.println(s);
				if(start){
					//se non trova che all'inizio c'e' un > allora problema
					if((s.substring(0,1)).equals(">")){
						name=s.substring(1).split("\\$")[0].split("pos")[0].replaceAll("[\r\n]","");
						start=false;
						//						if(firstSequence){ //se e' la prima subopt segnati il nome
						//							firstName = s.substring(1).split("�")[0];
						//							firstSequence = false;
						//						}

					}else{
						System.out.println("Check file format !\ns.substring(0,1) not equal '>' !!\n" + s.substring(0,1));
						System.exit(-1);
					}

				}else{ //non stiamo all'inizio
					String firstChar=s.substring(0,1);
					if(firstChar.equals(">")){ //se e' un intestazione fasta -> aggiungi la precedente
						//						System.err.println(nuc + "\n" + seq + "\n" + dotB);

						if(subopt){ //se nel passo precedente e' stata riconosciuta una subopt allora non aggiungere un nuovo
							//Motif ad inputSequences, ma riempi l'arrayList di String dell'ultimo messo
							inputSequences.get(inputSequences.size()-1).getSequenceList().add(seq);

						}else{
							//aggiungi la subopt -0- a inputSequences
							inputSequences.add(new Motif(name, nuc, dotB, seq,0,0));// (aggiunge quella precedente!!)
							firstName = name;
							inputSequences.get(inputSequences.size()-1).addSuboptMask();


						}



						name = s.substring(1).split("\\$")[0].split("pos")[0].replaceAll("[\r\n]", "");
						seq="";
						nuc="";
						dotB="";


						if(name.equals(firstName)){//se la nuova sequenza e' una subopt di quella sopra
							subopt = true;
						}else{
							subopt = false;
						}

					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[acgurymkswbdhvntACGURYMKSWBDHVNT]*$")){ 
						//controllo per presenza di riga di sequenza
						nuc += s.trim().replaceAll("[\r\n]", "");
					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[.()]*$")){
						//controllo per presenza riga dotBracket
						dotB+= ((s.trim()).replaceAll("[\r\n]",""));
					}else{
						seq+= ((s.trim()).replaceAll("[\r\n]",""));
					}

				}
			}

			//quando esci segnati l'ULTIMA sequenza del file
			if(subopt){
				inputSequences.get(inputSequences.size()-1).getSequenceList().add(seq);
			}else{
				inputSequences.add(new Motif(name, nuc,dotB, seq,0,0));
				inputSequences.get(inputSequences.size()-1).addSuboptMask();

			}
		}catch(IOException ioException){ioException.printStackTrace();}

		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	static public void scream(int status){
		System.err.println("AAAAAAARGH!");
		System.exit(status);
	}

	public static void deletePrevious(String myDirectoryPath) throws IOException{


		File dir = new File(myDirectoryPath);
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File child : directoryListing) {
				// Do something with child
				deleteFolder(child);
			}
		}
	}

	private static void deleteFolder(File fin) throws IOException{
		for (File file : fin.listFiles()) {
			FileDeleteStrategy.FORCE.delete(file);
		}   
	}

	public static void moveBestRuns(String baseName, int mask, int bestRunIdx,
			String outDad) throws IOException {
		//TODO copy best benchmark in final folder, use bestRunIdx to manage filenames basename_m<mask>_run<bestRunIdx>
		String sourcename= outDad +"/mask"+ mask + "/" + baseName + "_m" + mask + "_run" + bestRunIdx + ".txt"; 
		String destname=outDad +"/motifs/" + baseName + "_m" + mask + "_run" + bestRunIdx + ".txt"; 

		File source = new File(sourcename);
		File dest = new File(destname);
		copyUsingFileChannels(source,dest);

	}
	
	public static void moveBestRunsLogo(String baseName, int mask, int bestRunIdx,
			String outDad) throws IOException {
		//TODO copy best benchmark in final folder, use bestRunIdx to manage filenames basename_m<mask>_run<bestRunIdx>
		String sourcename= outDad +"/mask"+ mask + "/" + baseName + "_m" + mask + "_run" + bestRunIdx + "_wl.fa"; 
		String destname=outDad +"/motifs/" + baseName + "_m" + mask + "_run" + bestRunIdx + "_wl.fa"; 

		File source = new File(sourcename);
		File dest = new File(destname);
		copyUsingFileChannels(source,dest);

	}

	public static void moveBestExcel(String baseName,int mask, int bestRunIdx,
			String outDad) throws IOException {
		//TODO copy best benchmark in final folder, use bestRunIdx to manage filenames basename_m<mask>_run<bestRunIdx>
		String sourcename= outDad + "/mask"+ mask + "/excel_" + bestRunIdx + ".txt"; 
		String destname=outDad +"/motifs/excel_" + baseName + "_run" + bestRunIdx + ".txt"; 

		File source = new File(sourcename);
		File dest = new File(destname);
		copyUsingFileChannels(source,dest);

	}
	
	private static void copyUsingFileChannels(File source, File dest) throws IOException {
		try {
			FileUtils.copyFile( source, dest);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void maskedExit() {
		// TODO Auto-generated method stub
		System.out.println("No unmasked sequences left");
		System.exit(0);
	}

	static void readInputForSearch(String path, ArrayList<Motif> inputSequences){
		FileReader f = null;
		try{
			//apro file
			f = new FileReader(path);
		}catch (IOException ioException){
			ioException.printStackTrace();
		}
	
		try{
			BufferedReader reader = new BufferedReader(f);
			String s,name="",seq="", firstName="", nuc="", dotB="";
			boolean start=true;
			boolean subopt = false;
	
			while ((s=reader.readLine())!=null){
	
				//System.out.println(s);
				if(start){
					//se non trova che all'inizio c'e' un > allora problema
					if((s.substring(0,1)).equals(">")){
						name=s.substring(1).split("\\$")[0].split("pos")[0].replaceAll("[\r\n]","");
						start=false;
						//						if(firstSequence){ //se e' la prima subopt segnati il nome
						//							firstName = s.substring(1).split("�")[0];
						//							firstSequence = false;
						//						}
	
					}else{
						System.out.println("Check file format !\ns.substring(0,1) not equal '>' !!\n" + s.substring(0,1));
						System.exit(-1);
					}
	
				}else{ //non stiamo all'inizio
					String firstChar=s.substring(0,1);
					if(firstChar.equals(">")){ //se e' un intestazione fasta -> aggiungi la precedente
						//						System.err.println(nuc + "\n" + seq + "\n" + dotB);
	
						if(subopt){ //se nel passo precedente e' stata riconosciuta una subopt allora non aggiungere un nuovo
							//Motif ad inputSequences, ma riempi l'arrayList di String dell'ultimo messo
							inputSequences.get(inputSequences.size()-1).getSequenceList().add(seq);
	
						}else{
							//aggiungi la subopt -0- a inputSequences
							inputSequences.add(new Motif(name, nuc, dotB, seq,0,0));// (aggiunge quella precedente!!)
							firstName = name;
							inputSequences.get(inputSequences.size()-1).addSuboptMask();
	
	
						}
	
	
	
						name = s.substring(1).split("\\$")[0].split("pos")[0].replaceAll("[\r\n]", "");
						seq="";
						nuc="";
						dotB="";
	
	
						if(name.equals(firstName)){//se la nuova sequenza e' una subopt di quella sopra
							subopt = true;
						}else{
							subopt = false;
						}
	
					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[acgurymkswbdhvntACGURYMKSWBDHVNT]*$")){ 
						//controllo per presenza di riga di sequenza
						//nuc += s.trim().replaceAll("[\r\n]", "");
					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[.()]*$")){
						//controllo per presenza riga dotBracket
						//dotB+= ((s.trim()).replaceAll("[\r\n]",""));
					}else{
						seq+= ((s.trim()).replaceAll("[\r\n]",""));
					}
	
				}
			}
	
			//quando esci segnati l'ULTIMA sequenza del file
			if(subopt){
				inputSequences.get(inputSequences.size()-1).getSequenceList().add(seq);
			}else{
				inputSequences.add(new Motif(name, nuc,dotB, seq,0,0));
				inputSequences.get(inputSequences.size()-1).addSuboptMask();
	
			}
		}catch(IOException ioException){ioException.printStackTrace();}
	
		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}

}
