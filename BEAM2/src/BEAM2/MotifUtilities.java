package BEAM2;

import java.math.BigDecimal;
import java.util.ArrayList;

public class MotifUtilities {

	public static void computePartials(PSSM pssm, MotifHandler mh, double[][] matrix, int motifWidth, double bonus){
		//input PSSM,mh. Il parziale e' lo score della sequenza con la pssm, passi successivi: tenere solo quelli vicino alla media
		int nucleoMatrix[][]= new int [4][motifWidth]; //Matrice frequenza nucleotidi
		for (int i=0;i<4;i++)
			for(int j=0;j<motifWidth;j++)
				nucleoMatrix[i][j]=0;
		for (Motif m: mh.getListMotif()){
			//Riempimento matrice frequenze nucleotidi
			nucleoMatrix=fillNucleo(nucleoMatrix,m.getNucleotidesMotif());
			//per ogni sequence in mh
			double tmp=0.0;
			for(int j=0; j < motifWidth;j++){
				//scorri ogni colonna e calcola lo score come la media pesata per le occorrenze 
				for(PSSMCell pos:pssm.getMatrix().get(j)){
					tmp+=BEARManager.substitutionScore(pos.name, m.extractMotifFromSequence().charAt(j), matrix)*pos.occurrence;
				}
			}
			//Score parziale in attesa del bonus, se non c'è il bonus resta così
			m.setPartial(tmp);
		}
		//Se non c'è bonus, si risparmia in tempo cpu. Altrimenti risetta lo score parziale
		//considerando le frequenze dei nucleotidi del motif moltiplicato per il bonus
		if (bonus!=0.0){
			String nt="ACUG";
			for (Motif m: mh.getListMotif()){
				double tmp=0.0;
				for(int j=0; j < motifWidth;j++){
					tmp+=(nucleoMatrix[nt.indexOf(m.getNucleotidesMotif().charAt(j))][j]/mh.cardinality())*bonus;
				}
				m.setPartial(m.getPartial()+tmp);
			}
		}
	}
	
	//Metodo per il riempimento della matrice delle frequenze dei nucleotidi
	private static int[][] fillNucleo(int matrix[][], String nucleoMotif){
		String nt="ACUG";
			for(int j=0;j<nucleoMotif.length();j++){
				matrix[nt.indexOf(nucleoMotif.charAt(j))][j]++;
			}
		return matrix;
	}
	
	public static double computeScoreVsPSSM(PSSM pssm, Motif m, double[][] matrix, int motifWidth){
		//input PSSM,mh. Il parziale � lo score della sequenza con la pssm, passi successivi: tenere solo quelli vicino alla media
		int idx=0;
		double max=-9999.0;
		for(int i=0; i < (m.getSequence().length()-motifWidth); i++ ){
			double tmp=0.0;
			for(int j=0; j < motifWidth;j++){
				//				scorri ogni colonna e calcola lo score come la media pesata per le occorrenze 

				for(PSSMCell pos:pssm.getMatrix().get(j)){
					tmp+=BEARManager.substitutionScore(pos.name, m.getSequence().charAt(j+i), matrix)*pos.occurrence;
				}
			}
			//tmp2 contiene il punteggio della finestra
			if(tmp > max){
				max=tmp;
				idx=i;
			}
		}
		m.setMotifStart(idx);
		m.setMotifEnd(idx+motifWidth);
		double partial_ = max;
		return partial_;

	}

	static public double max(double[] array){
		double max = -999.0;
		for(int i=0;i<array.length; i++){
			if (array[i] > max){
				max = array[i];
			}
		}
		return max;
	}

	public static void clean(MotifHandler mh, double thr){
		double mediaPart = 0.0;
		for(Motif m: mh.getListMotif()){ //questa parte puo' essere integrata al computePartials e passato direttamente la media qua
			mediaPart += m.getPartial();
		}
		mediaPart /= mh.cardinality();
		//Ho ottenuto la media dei parziali, ora identifico quelli con punteggio meno del thr% della media

		for(int i=(mh.cardinality()-1); i >=0 ; i--){
			if (mh.getObjectMotif(i).getPartial() < thr*mediaPart){
				//System.err.println(mh.getObjectMotif(i).getPartial());
				mh.removeMotif(i);
			}else{
				//System.out.println(mh.getObjectMotif(i).getPartial());
			}
		}
	}

	static public int maxIndex(double[] array){
		double max = -999.0;
		int maxI = 0;
		for(int i=0;i<array.length; i++){
			if (array[i] >= max){
				max = array[i];
				maxI = i;
			}
		}
		return maxI;
	}

	public static BigDecimal truncateDecimal(double x,int numberofDecimals)
	{
		if ( x > 0) {
			return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_FLOOR);
		} else {
			return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_CEILING);
		}
	}

	public static void setBranchingZero(double[][] mbr){
		mbr[48][48]=0.0;
	}

	public static void reshapeInput(ArrayList<Motif> inputSequences) {
		//controlla inputSequences se ci sono duplicati causati da boh. In caso toglili
		//Da lanciare una volta a inizio run
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<Motif> toBeRem = new ArrayList<Motif>();
		for (Motif m: inputSequences){
			if (list.contains(m.getName()) ){
				toBeRem.add(m);
			}else{
				list.add(m.getName());
			}
		}
		for (Motif m: toBeRem){
			inputSequences.remove(m);
		}

	}



}
